require 'spec_helper'

describe JobOffer do
  subject(:job) { described_class.new(title: 'Programmer Job') }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:user) }
    it { is_expected.to respond_to(:required_experience) }
  end

  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.new(location: 'a location')
      end
    end

    it 'should be valid when title is not blank' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer).to be_valid
    end

    it 'should be invalid when experience is negative' do
      check_validation(:required_experience, 'Required experience must be greater than or equal to 0') do # rubocop: disable Metrics/LineLength
        described_class.new(title: 'name', required_experience: -1)
      end
    end
  end
end
